#!/usr/bin/env bash

# unofficial bash strict mode
set -euo pipefail
IFS=$'\n\t'

claudia create \
  --region us-west-2 \
  --deploy-proxy-api \
  --handler lambda.handler \
  --version development