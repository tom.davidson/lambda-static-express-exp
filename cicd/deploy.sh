#!/usr/bin/env bash

# unofficial bash strict mode
set -euo pipefail
IFS=$'\n\t'

EVENT=$npm_lifecycle_event
APPNAME=$npm_package_name
BRANCH=$(git symbolic-ref --short HEAD)
DEVENV=$BRANCH
echo "$EVENT" "$APPNAME" from "$BRANCH"

DEPLOYCMD="claudia update --version "

if [[ "$EVENT" == "deploy:prod" ]]; then
  if [[ "$BRANCH" == "master" ]]; then
     eval $DEPLOYCMD production
  fi

elif [[ "$EVENT" == "deploy" ]]; then
  if [[ "$BRANCH" == "master" ]]; then
    eval $DEPLOYCMD stage
  else 
    eval $DEPLOYCMD $DEVENV
  fi

else
  echo "Livecycle event must be set."
  exit 1;

fi

wait;